package com.rivale.milemarker;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rivale.milemarker.services.RouteService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private RouteService routeService;

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    //minimum distance (in meters) between two points before calling google directions
    private static final float MIN_DISTANCE = 100;
    private final int ACCESS_LOCATION = 0;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private Location mLocation;
    private LatLng mLatLng;

    private Location newLocation;
    private LatLng newLatLng;

    private TextView textview_output;

    private List<LatLng> positions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        routeService = RouteService.getInstance(getApplicationContext());

        textview_output = (TextView) findViewById(R.id.textview_output);
        textview_output.setText("i need more points");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (mGoogleApiClient == null) {
            rebuildGoogleApiClient();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private synchronized void rebuildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addApi(AppIndex.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        drawPosition();
    }

    private List<Marker> previousMarkers = new ArrayList<Marker>();

    private void drawPosition() {
        // Add a marker in Roma and move the camera
        mLatLng = new LatLng(41.9102415, 12.3959151);

        mLocation = new Location("");
        mLocation.setLatitude(41.9102415);
        mLocation.setLongitude(12.3959151);

        MarkerOptions romaMarker = new MarkerOptions().position(mLatLng).title("Marker in Rome");
        drawPosition(mLatLng, romaMarker);
    }

    private void drawPosition(LatLng newLatLng) {
        drawPosition(newLatLng, new MarkerOptions().position(newLatLng).title("Marker GEOLOCALIZED"));
    }

    boolean alreadyZoomed = false;

    private void drawPosition(LatLng newLatLng, MarkerOptions markerOptions) {

        for (Marker oldMarker : previousMarkers) {
            oldMarker.remove();
        }
        if (markerOptions != null) {
            previousMarkers.add(mMap.addMarker(markerOptions));
        }

        float zoom = 14;
        if (!alreadyZoomed) {
            alreadyZoomed = true;
            // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(newLatLng)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom
                    .bearing(0)                // Sets
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);
        } else {
            zoom = mMap.getCameraPosition().zoom;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLatLng, zoom));
        }

        //keep only 2 positions.. i don't need more
        addPosition(newLatLng);

        try {
            updateDrivingStatus(newLatLng);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addPosition(LatLng newLatLng) {
        positions.add(newLatLng);
        if (positions.size() >= 3) {
            positions.remove(0);
        }
    }

    private void updateDrivingStatus(LatLng current) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("lat",current.latitude);
        jsonObject.put("lon",current.longitude);
        new UpdateDrivingStatusAsyncTask().execute(jsonObject);
    }

    @Override
    public void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Maps Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.rivale.milemarker/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        Action viewAction = Action.newAction(
                Action.TYPE_VIEW,
                "Maps Page",
                Uri.parse("http://host/path"),
                Uri.parse("android-app://com.rivale.milemarker/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
        mGoogleApiClient.disconnect();
    }

    private class UpdateDrivingStatusAsyncTask extends AsyncTask<JSONObject, String, String> {

        @Override
        protected String doInBackground(JSONObject... params) {
            return routeService.updateDrivingStatus(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            textview_output.setText(result);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        retrieveCurrentPosition();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mGoogleApiClient.isConnected()) {
            retrieveCurrentPosition();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()){
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            retrieveCurrentPosition();
        }
    }

    private void retrieveCurrentPosition() {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    ACCESS_LOCATION);

            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        newLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        //if i have a location
        if(newLocation!=null){

            newLatLng = new LatLng(newLocation.getLatitude(), newLocation.getLongitude());

            if (newLocation != null && enoughDistance(newLatLng) > MIN_DISTANCE) {
                mLocation = newLocation;
                drawPosition(newLatLng);
            }
        }
    }

    private float enoughDistance(LatLng newLatLng) {

        LatLng last = positions.get(positions.size() - 1);

        Location oldOne = new Location("old");
        oldOne.setLatitude(last.latitude);
        oldOne.setLongitude(last.longitude);

        Location newOne = new Location("new");
        newOne.setLatitude(newLatLng.latitude);
        newOne.setLongitude(newLatLng.longitude);

        return oldOne.distanceTo(newOne);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    retrieveCurrentPosition();
                } else {
                    Toast.makeText(this, "You have to give me permissions!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "Connection suspended", Toast.LENGTH_LONG).show();
    }
}