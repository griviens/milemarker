package com.rivale.milemarker.beans;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ale on 01/03/2016.
 */
public class Destination {

    public Destination(LatLng destination,String name){
        this.destination = destination;
        this.name = name;
    }

    private LatLng destination;
    private String name;

    public LatLng getDestination() {
        return destination;
    }

    public String getName() {
        return name;
    }
}